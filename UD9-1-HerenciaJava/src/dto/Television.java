package dto;

public class Television extends Electrodomestico {
	
	// valores constantes
	private static final int resolucion20 = 20;
	private static final boolean sintonizadorFalse = false;
		
	// Atributos	
	private int resolucion;
	private boolean sintonizador;
	
	
	//Constroctores
	public Television() {
		this.resolucion = resolucion20;
		this.sintonizador = sintonizadorFalse;
	}


	public Television(double precioBase, double peso) {
		super(precioBase, peso);
		this.resolucion = resolucion20;
		this.sintonizador = sintonizadorFalse;
	}


	public Television(int resolucion, boolean sintonizador, double precioBase, String color, char consumo, double peso) {
		super(precioBase, color, consumo, peso);
		this.resolucion = resolucion;
		this.sintonizador = sintonizador;
	}
	
	//Metodos
	public double precioFinal() {
		double precio = precioBase;
		if (peso>=80) {
			precio=precio+100.0;
		} else if(precio>=50) {
			precio=precio+80.0;
		} else if(precio>=20) {
			precio=precio+50.0;
		}else if(precio>=0) {
			precio=precio+10.0;
		}
		switch(consumo) {
			case 'A':
				precio=precio+100.0;
				break;
			case 'B':
				precio=precio+80.0;
				break;
			case 'C':
				precio=precio+60.0;
				break;
			case 'D':
				precio=precio+50.0;
				break;
			case 'E':
				precio=precio+30.0;
				break;
			case 'F':
				precio=precio+10.0;
				break;
		}
		if(resolucion >= 40) {
			double precioIncrementado = precio * 0.30;
			precio += precioIncrementado;
		}
		if(sintonizador) {
			precio += 50;
		}
		return precio;
	}
	
	//Getters and Setters

	/**
	 * @return the resolucion
	 */
	public int getResolucion() {
		return resolucion;
	}
	
	/**
	 * @param resolucion the resolucion to set
	 */
	public void setResolucion(int resolucion) {
		this.resolucion = resolucion;
	}


	/**
	 * @return the sintonizador
	 */
	public boolean isSintonizador() {
		return sintonizador;
	}


	/**
	 * @param sintonizador the sintonizador to set
	 */
	public void setSintonizador(boolean sintonizador) {
		this.sintonizador = sintonizador;
	}

	@Override
	public String toString() {
		return "Television [resolucion=" + resolucion + ", sintonizador=" + sintonizador + "]";
	}
	

}
