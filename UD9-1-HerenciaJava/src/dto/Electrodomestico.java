package dto;

public class Electrodomestico {
	
	final char DConsumo = 'F';
	final double DPrecio= 100.0;
	final String DColor = "Blanco";
	final double DPeso = 5.0;
	
	//Atributos
	protected double precioBase;
	private String color;
	protected char consumo;
	protected double peso;
	
	//Constantes
	public Electrodomestico() {
		this.precioBase = DPrecio;
		this.color = DColor;
		this.consumo = DConsumo;
		this.peso = DPeso;
	}
	
	
	public Electrodomestico(double precioBase, double peso) {

		this.precioBase = precioBase;
		this.color = DColor;
		this.consumo = DConsumo;
		this.peso = peso;
	}


	public Electrodomestico(double precioBase, String color, char consumo, double peso) {
		super();
		this.precioBase = precioBase;
		this.color=comprobarColor(color);
		this.consumo=comprobarConsumoEnergetico(consumo);
		this.peso = peso;
	}


	@Override
	public String toString() {
		return "Electrodomestico [precioBase=" + precioBase + ", color=" + color + ", consumo=" + consumo + ", peso="
				+ peso + "]";
	}
	
	
	private  String comprobarColor(String color) {
		String colorF;
		if(color.equalsIgnoreCase("blanco") || color.equalsIgnoreCase("negro") || color.equalsIgnoreCase("azul") || color.equalsIgnoreCase("rojo") || color.equalsIgnoreCase("gris")) {
			colorF = color;
		} else {
			colorF = DColor;
		}
		return colorF;
	}
	
	private char comprobarConsumoEnergetico(char letra) { 
		char consumoF;
		if(letra=='A'||letra=='B'||letra=='C'||letra=='D'||letra=='E'||letra=='F') {
			consumoF = letra;
		} else {
			consumoF = DConsumo;
		}
		return consumoF;
	}

	public double precioFinal() { //Devolvemos el precio finn calculado
		double precio = precioBase;
		if (peso>=80) {
			precio=precio+100.0;
		} else if(precio>=50) {
			precio=precio+80.0;
		} else if(precio>=20) {
			precio=precio+50.0;
		}else if(precio>=0) {
			precio=precio+10.0;
		}
		switch(consumo) {
			case 'A':
				precio=precio+100.0;
				break;
			case 'B':
				precio=precio+80.0;
				break;
			case 'C':
				precio=precio+60.0;
				break;
			case 'D':
				precio=precio+50.0;
				break;
			case 'E':
				precio=precio+30.0;
				break;
			case 'F':
				precio=precio+10.0;
				break;
		}
		return precio;
	}


	//Getters and Setters
	public double getPrecioBase() {
		return precioBase;
	}


	public String getColor() {
		return color;
	}


	public char getConsumo() {
		return consumo;
	}


	public double getPeso() {
		return peso;
	}

}
