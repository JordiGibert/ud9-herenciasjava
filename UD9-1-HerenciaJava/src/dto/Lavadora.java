package dto;

public class Lavadora extends Electrodomestico {
	final double cargaD=5.0;
	
	//Atributos
	private double carga;

	//Constantes
	public Lavadora() {
		super();
		this.carga=cargaD;
		// TODO Auto-generated constructor stub
	}

	public Lavadora(double precioBase, double peso) {
		super(precioBase, peso);
		this.carga=cargaD;
		// TODO Auto-generated constructor stub
	}

	public Lavadora(double precioBase, String color, char consumo, double peso, double carga) {
		super(precioBase, color, consumo, peso);
		
		// TODO Auto-generated constructor stub
	}
	
	//Devolver el precio final calculado
	public double precioFinal() {
		double precio = precioBase;
		if (peso>=80) {
			precio=precio+100.0;
		} else if(peso>=50) {
			precio=precio+80.0;
		} else if(precio>=20) {
			precio=precio+50.0;
		}else if(precio>=0) {
			precio=precio+10.0;
		}
		switch(consumo) {
			case 'A':
				precio=precio+100.0;
				break;
			case 'B':
				precio=precio+80.0;
				break;
			case 'C':
				precio=precio+60.0;
				break;
			case 'D':
				precio=precio+50.0;
				break;
			case 'E':
				precio=precio+30.0;
				break;
			case 'F':
				precio=precio+10.0;
				break;
		}
		
		if(carga>=30) {
			precio=precio+50.0;
		}
		return precio;
	}

	public double getCarga() {
		return carga;
	}
	
}
