import dto.Electrodomestico;
import dto.Lavadora;
import dto.Television;

import java.util.ArrayList;
public class MainApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Electrodomestico microones=new Electrodomestico();
		Electrodomestico forn=new Electrodomestico(69.9,100.2);
		Electrodomestico nevera=new Electrodomestico(120.0,"NegRo",'Y',132.3);
		Lavadora motorola= new Lavadora(120.0,"rosa",'C',21.3,22.0);
		Lavadora lg= new Lavadora(200.0,"blanca",'B',21.3,43.0);
		
		Electrodomestico nevera2=new Electrodomestico(220.0,"blanca",'C',30);
        Electrodomestico clima=new Electrodomestico(320.0,"blanco",'A',17);

        Television samsung = new Television(32,true,350,"Negro",'A',14.65);
        Television philips = new Television(42,true,470,"Negro",'C',22.40);
        Television sony = new Television(32,false,150,"blanca",'A',18.22);
		ArrayList<Electrodomestico> listaElectrodomesticos =new ArrayList<Electrodomestico>();
		listaElectrodomesticos.add(microones);
		listaElectrodomesticos.add(forn);
		listaElectrodomesticos.add(nevera);
		listaElectrodomesticos.add(motorola);
		listaElectrodomesticos.add(lg);
		listaElectrodomesticos.add(nevera2);
		listaElectrodomesticos.add(clima);
		listaElectrodomesticos.add(philips);
		listaElectrodomesticos.add(sony);
		listaElectrodomesticos.add(samsung);
		
		double total=0.0;
		for(int i=0; i<listaElectrodomesticos.size(); i++) {
			total=total+listaElectrodomesticos.get(i).precioFinal();
		}
		System.out.println(total);
		
	}

}
